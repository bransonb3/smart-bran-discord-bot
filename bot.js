const Discord = require('discord.js');
const client = new Discord.Client();
const versionNum = "1.14.2";
let token = process.argv[2];

client.on('message', msg => {
    // so the bot doesn't reply to itself
    if (msg.author === client.user){
        return;
    }

    if(msg.content === "*info"){
        msg.channel.send(info());
    }

    // returns pong
    if (msg.content === '*ping') {
        msg.channel.send('Pong!');
    }

    // alternating case
    if (msg.content.startsWith('*s ')) {
        msg.channel.send(altCase(msg.content));
    }

    // says a message aloud
    if (msg.content.startsWith('*tts ')) {
        let receiveMessage = String(msg.content).replace('*tts ', '');
        msg.channel.send(receiveMessage, {tts: true});
    }

    // translates a message into uwu
    if (msg.content.startsWith('*uwu ')) {
        msg.channel.send(translate(msg.content));
    }

    // translate into uwu and says the message aloud
    if (msg.content.startsWith('*uwutts ')) {
        msg.channel.send(translate(msg.content), {tts: true});
    }

    // converts a message into natto characters
    if (msg.content.startsWith('*natotts ')) {
        msg.channel.send(lengthChecker(natoTranslator(msg.content)), {tts: true});
    }

    // flips a coin
    if (msg.content === ("*coin")) {
        if (Math.round(Math.random()) === 0){
            msg.channel.send("Heads!");
        } else {
            msg.channel.send("Tails!");
        }
    }

    // plays rock, paper, scissors with the user
    if (msg.content.startsWith("*rps ")) {
        let userChoice = String(msg.content).replace("*rps ","").toLowerCase();
        let comChoice = computerChoice();

        msg.channel.send("Computer Choice: " + comChoice + "\n" + rps(userChoice, comChoice));
    }

    // fetches a dad joke from the internet
    if (msg.content === "*dadjoke"){
        let sendMessage = "";
        const https = require('https');
        const options = {
            hostname: 'icanhazdadjoke.com',
            port: 443,
            path: '/',
            method: 'GET',
            headers: {
                'Accept': 'text/plain'
            }
        };
        const req = https.request(options, (res) => {
            res.on('data', (d) => {
                sendMessage = (d.toString());
                if (sendMessage.length < 2000){
                    msg.channel.send(sendMessage);
                }else{
                    msg.channel.send("The response to your last request was too long to send.");
                }
            })
        });
        req.end()
    }

    // displays what the bot can do
    if (msg.content === "*help"){
        msg.channel.send(help());
    }
});

function lengthChecker(message) {
    if (message.length > 2000) {
        return "This message is too long to translate.";
    } else {
        return message;
    }
}

function help() {
    return new Discord.RichEmbed()
        .addField("*ping", "Responses with Pong!")
        .addField("*s text", "SaRcAsM TeXt cOnVeRtEr")
        .addField("*tts text", "Reads out the text")
        .addField("*natotts text", "Converts text to nato alphabet then reads it out")
        .addField("*coin", "Flips a coin")
        .addField("*rps [r,p,s]", "Rock, Paper, Scissors. *rps r, *rps p, *rps s")
        .addField("*uwu text", "UwU Translator")
        .addField("*uwutts txt", "TTS UwU Translator")
        .addField("*dadjoke", "Returns a dad joke")
        .addField("*info", "Smart Bran info")
        .addField("*help", "This message");
}

function info() {
    return new Discord.RichEmbed()
        .setTitle('Smart Bran� Info')
        .setURL('https://bitbucket.org/bransonb3/smart-bran-discord-bot/')
        .addField("Author", "Branson Buchanan and Hayden Key")
        .addField("Version", versionNum);
}

function altCase(param) {
    let letterCount = 0;
    let sendMessage = "";
    let receiveMessage = String(param).replace('*s ', '').split('');

    if (param === "") {
        return "Please enter something after \"*s \" so that I can translate it.";
    }

    for (let ltr of receiveMessage) {
        if (letterCount % 2 === 0) {
            sendMessage = sendMessage + ltr.toUpperCase();
        } else {
            sendMessage = sendMessage + ltr.toLowerCase();
        }
        letterCount++;
    }

    return sendMessage;
}

function natoTranslator(message) {
    let receive = String(message).replace('*natotts ', '')
        .toLowerCase().split('');
    let send = "";

    let nattoDict = {
        "a": "alpha",
        "b": "bravo",
        "c": "charlie",
        "d": "delta",
        "e": "echo",
        "f": "foxtrot",
        "g": "golf",
        "h": "hotel",
        "i": "india",
        "j": "juliet",
        "k": "kilo",
        "l": "lima",
        "m": "mike",
        "n": "november",
        "o": "oscar",
        "p": "papa",
        "q": "quebec",
        "r": "romeo",
        "s": "sierra",
        "t": "tango",
        "u": "uniform",
        "v": "victor",
        "w": "whiskey",
        "x": "xray",
        "y": "yankee",
        "z": "zulu",
    };

    for (let letter of receive) {
        send += nattoDict[letter] + " ";
    }

    return lengthChecker(send);
}

// Compare user choice vs computer choice
function rps(userChoice, computerChoice) {
    if (userChoice === computerChoice) {
        return "It's a tie!";
    }

    if (userChoice === "r") {
        if (computerChoice === "s") {
            // rock wins
            return "You win!";
        } else {
            // paper wins
            return "You lose! Try again.";
        }
    }

    if (userChoice === "p") {
        if (computerChoice === "r") {
            // paper wins
            return "You win!";
        } else {
            // scissors wins
            return "You lose! Try again.";
        }
    }

    if (userChoice === "s") {
        if (computerChoice === "r") {
            // rock wins
            return "You lose! Try again.";
        } else {
            // scissors wins
            return "You win!";
        }
    }
    return `Please enter the letter "r" for rock, "s" for scissors, or "p" for paper`;
}

function computerChoice() {
    let choice = Math.floor(Math.random() * 3);

    switch(choice) {
        case 0:
            return "s";
        case 1:
            return "r";
        case 2:
            return "p";
    }
}

function translate(message) {
    message = message.toString();
    message = message.replace("*uwu ", "");
    message = message.replace("*uwutts ", "");

    let sender = "";
    let letter = "";
    let uwuDict = {
        "r": "w",
        "R": "W",
        "th": "d",
        "TH": "D",
        "l": "w",
        "L": "W",
        ".": ". uwu"
    };

    for (let i = 0; i < message.length; i++) {
        letter = message[i];
        let doubleLetter = false;

        if (letter.toLowerCase() === "n" || letter.toLowerCase() === "m" && message.length - 1 > i) {
            let nextLetter = message[i+1];
            if (nextLetter.toLowerCase() === "o") {
                sender += letter + "yo";
                doubleLetter = true;
                i++;
            }
        }


        if (letter.toLowerCase() === "f" && message.length - 1 > i) {
            let nextLetter = message[i+1];
            if (nextLetter.toLowerCase() === "u") {
                sender += "fwu";
                doubleLetter = true;
                i++;
            }
        }

        if (letter in uwuDict) {
            sender += uwuDict[letter];
        } else if (!doubleLetter) {
            sender += letter;
        }
    }

    if (sender.length > 2000) {
        return "dis message is too wong to twanswate. >w<";
    } else {
        return sender
    }
}

client.login(token);